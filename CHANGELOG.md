# Change Log

- [Change Log](#change-log)
  - [v0.12](#v012)
    - [Modified](#modified)
  - [v0.11](#v011)
    - [Modified](#modified-1)
  - [v0.10](#v010)
    - [Modified](#modified-2)
  - [v0.9](#v09)
    - [Modified](#modified-3)
  - [v0.8](#v08)
    - [Modified](#modified-4)
  - [v0.7](#v07)
    - [Modified](#modified-5)
  - [v0.6](#v06)
    - [Modified](#modified-6)
  - [v0.5](#v05)
    - [Modified](#modified-7)
  - [v0.4](#v04)
    - [Modified](#modified-8)
  - [v0.3](#v03)
    - [Added](#added)
    - [Modified](#modified-9)
  - [v0.2](#v02)
    - [Modified](#modified-10)
  - [v0.1](#v01)
    - [Added](#added-1)

## v0.12

### Modified

- docs
  - Update
- main.py
  - Add compression to rsync command line arguments
  - Change table layout according to new input data
  - Skip empty directories during rsync process
- README.md
  - Update

## v0.11

### Modified

- docs
  - Update
- main.py
  - Fix indention in html code
  - Fix source and target variable
  - New algorithm to extragt tags from directory and filename structure
  - Use new data table variant
- requirements.txt
  - Update

## v0.10

### Modified

- docs
  - Update
- main.py
  - Bugfix

## v0.9

### Modified

- docs
  - Update
- main.py
  - Bugfix

## v0.8

### Modified

- docs
  - Update
- .gitignore
  - Add data directory
- main.py
  - Add dictionaries
  - Add function get_directory()
  - Change title to subtitle
  - Change to directory-wide dataset
  - Make dictionaries global
  - Minor bugfix

## v0.7

### Modified

- docs
  - Update
- .gitignore
  - Bugfix
- main.py
  - Add more variables
  - Change sorting
  - Minor bugfix

## v0.6

### Modified

- docs
  - Update
- main.py
  - Bugfix

## v0.5

### Modified

- docs
  - Update
- main.py
  - Bugfix

## v0.4

### Modified

- docs
  - Update
- .gitignore
  - Update
- main.py
  - Bugfix

## v0.3

### Added

- docs/requirements.md

### Modified

- .gitignore
  - Update
- main.py
  - Change function parameters
  - Change table clustering / sorting
  - Drop kinematic region 0-350 GeV for VBF plots
  - Generate www directories with index.html files
  - Minor changes
  - Move code into a match function

## v0.2

### Modified

- .gitignore
  - Update
- docs/main.html
  - Update
- main.py
  - Add contact information
  - Add link to .pdf
  - Add repo link
  - Change output filename to 'index.html'
  - Reorder columns

## v0.1

### Added

- docs/main.html
- .gitignore
- main.py
- README.md
- requirements.txt