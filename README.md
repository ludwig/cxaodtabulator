# CxAODTabulator

- [CxAODTabulator](#cxaodtabulator)
  - [Requirements](#requirements)
    - [Install](#install)
    - [Activate and update virtual environment](#activate-and-update-virtual-environment)
  - [Usage](#usage)
    - [Download plots](#download-plots)
    - [Run](#run)
    - [Upload plots to EOS](#upload-plots-to-eos)
      - [dev](#dev)
      - [release](#release)
    - [Update requirements](#update-requirements)
    - [Generate documentation](#generate-documentation)

## Requirements

### Install

To create a virtual environment with VS Code, press Strg + Shift + P to open the Command Palette. Use the following commands:

- [ ] `Python: Create Environment...`
- [ ] `Venv`
- [ ] select your global python interpreter
- [ ] `requirement.txt`

### Activate and update virtual environment

```
source .venv/bin/activate
pip install -r requirements.txt
```

## Usage

### Download plots

```
cd /home/$USER/git/cxaodtabulator
rsync -cmPrz --include '*/'  --include='*.pdf' --include='*.png' --exclude='*' ludwig@lxplus.cern.ch:/eos/atlas/atlaslocalgroupdisk/higgs/HHbbtautau/PlotForSteffen/ ./data/
```

### Run

```
python main.py
```

### Upload plots to EOS

#### dev

```
rsync -cPrz --delete /home/$USER/git/cxaodtabulator/www/ ludwig@lxplus.cern.ch:/eos/user/l/ludwig/www/cxaodplots-dev/
```

#### release

```
rsync -cPrz --delete /home/$USER/git/cxaodtabulator/www/ ludwig@lxplus.cern.ch:/eos/user/l/ludwig/www/cxaodplots/
```

### Update requirements

```
pip freeze > requirements.txt
```

### Generate documentation

```
mkdir -p docs
cd docs
python -m pydoc -w ../*.py
cd ..
pip-licenses --format=markdown --order=license --with-author --with-urls > docs/requirements.md
```
