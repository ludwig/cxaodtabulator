| Name            | Version | License                              | Author                                              | URL                                        |
|-----------------|---------|--------------------------------------|-----------------------------------------------------|--------------------------------------------|
| tzdata          | 2023.3  | Apache Software License              | Python Software Foundation                          | https://github.com/python/tzdata           |
| python-dateutil | 2.8.2   | Apache Software License; BSD License | Gustavo Niemeyer                                    | https://github.com/dateutil/dateutil       |
| numpy           | 1.25.2  | BSD License                          | Travis E. Oliphant et al.                           | https://www.numpy.org                      |
| pandas          | 2.0.3   | BSD License                          | The Pandas Development Team <pandas-dev@python.org> | https://pandas.pydata.org                  |
| pytz            | 2023.3  | MIT License                          | Stuart Bishop                                       | http://pythonhosted.org/pytz               |
| six             | 1.16.0  | MIT License                          | Benjamin Peterson                                   | https://github.com/benjaminp/six           |
| tabulate        | 0.9.0   | MIT License                          | Sergey Astanin <s.astanin@gmail.com>                | https://github.com/astanin/python-tabulate |
