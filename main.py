__version__ = '0.12'

import argparse
import glob
import inspect
import subprocess
from textwrap import indent

import pandas as pd
from tabulate import tabulate


def get_directory_tags(data: str) -> list:
    '''Returns directory tags.

    Args:
        data (str): Sets input data.

    Returns:
        list: Returns directory tags.
    '''

    # Set variables
    _data = data

    # Split
    data = _data.split('/')

    # Convert to tags
    tags = dict()
    shift = 0
    for level in range(len(data)):
        # Fix level 1
        if level == 1 and data[0] == 'HadHadPlots':
            shift += 1

        # # Fix level 2
        # if level == 2 and data[0] == 'LTT' and (data[1] == 'OS_SR' or data[1] == 'SS_SR') and (data[2] == 'FitInputs' or data[2] == 'FitInputs_prev' or data[2] == 'MVAInputs' or data[2] == 'MVAInputs_prev' or data[2] == 'ZVR'):
        #     shift += 1
        # if level == 2 and data[0] == 'SLT' and (data[1] == 'OS_SR' or data[1] == 'SS_SR') and (data[2] == 'FitInputs' or data[2] == 'FitInputs_prev' or data[2] == 'MVAInputs' or data[2] == 'MVAInputs_prev'):
        #     shift += 1

        # Set tags
        tags |= {f'directory_level_{level + shift}': data[level]}

    return tags


def get_filename_tags(data: str) -> list:
    '''Returns filename tags.

    Args:
        data (str): Sets input data.

    Returns:
        list: Returns filename tags.
    '''

    # Set variables
    _data = data

    # Replace and split
    data = _data.replace(
        '0_350mHH', '0-350mHH').replace('SMBDT_ggF_VBF', 'SMBDT-ggF-VBF').replace('corr_pt', 'corr-pt').split('_')

    # Convert to tags
    tags = dict()
    shift = 0
    for level in range(len(data)):
        # Fix replace
        data[level] = data[level].replace(
            '0-350mHH', '0_350mHH').replace('SMBDT-ggF-VBF', 'SMBDT_ggF_VBF').replace('corr-pt', 'corr_pt')

        # Fix level 3
        if level == 3 and data[level] != 'LL':
            shift += 1

        # Fix 'trafo60'
        if data[level] == 'trafo60':
            shift = 8 - level

        # Fix 'Log'
        if data[level] == 'Log':
            shift = 9 - level

        # Set tags
        tags |= {f'filename_level_{level + shift}': data[level]}

    return tags


def get_html(subtitle: str, data: pd.DataFrame, maintainer: pd.Series) -> str:
    '''Returns html code.

    Args:
        subtitle (str): Sets subtitle.
        data (pd.Series): Sets input data.
        maintainer (dict): Sets maintainer information. Required keys are 'username' and 'mail'.

    Returns:
        str: Returns html code.
    '''

    # Set variables
    _subtitle = subtitle
    _data = data
    _maintainer = maintainer

    # Set table
    table = '\n'.join([_ for _ in tabulate(_data,
                                           headers=_data.columns,
                                           tablefmt='unsafehtml',
                                           showindex=False).splitlines()[1:]])

    # Indent table
    table = indent(table, 32 * ' ')
    table = "<table id='table', class='display' style='width:100%'>\n" + table

    # Generate html code
    return inspect.cleandoc(f'''<html>
                                <head>
                                <link href='https://cdn.datatables.net/1.13.5/css/jquery.dataTables.min.css' rel='stylesheet'/>
                                <link href='https://cdn.datatables.net/searchpanes/2.2.0/css/searchPanes.dataTables.min.css' rel='stylesheet'/>
                                <link href='https://cdn.datatables.net/select/1.7.0/css/select.dataTables.min.css' rel='stylesheet'/>
                                <script src='https://code.jquery.com/jquery-3.7.0.js'></script>
                                <script src='https://cdn.datatables.net/1.13.5/js/jquery.dataTables.min.js'></script>
                                <script src='https://cdn.datatables.net/searchpanes/2.2.0/js/dataTables.searchPanes.min.js'></script>
                                <script src='https://cdn.datatables.net/select/1.7.0/js/dataTables.select.min.js'></script>
                                </head>
                                <body>
                                <h1>CxAODPlots</h1>
                                <h2>{_subtitle}</h2>
                                <script>
                                function getUrlParameter(name) {{
                                    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
                                    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
                                    var results = regex.exec(location.search);
                                    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
                                }};
                                        
                                $(document).ready(function() {{
                                    $('#table').DataTable({{
                                        scrollX: true,
                                        search: {{
                                            search: getUrlParameter('search')
                                        }},
                                        dom: 'Plfrtip',
                                        searchPanes: {{
                                            cascadePanes: true,
                                            preSelect: [
                                                {{
                                                    rows: [getUrlParameter('directory')],
                                                    column: 1
                                                }},
                                                {{
                                                    rows: [getUrlParameter('links')],
                                                    column: 2
                                                }},
                                                {{
                                                    rows: [getUrlParameter('directory_level_0')],
                                                    column: 3
                                                }},
                                                {{
                                                    rows: [getUrlParameter('directory_level_1')],
                                                    column: 4
                                                }},
                                                {{
                                                    rows: [getUrlParameter('directory_level_2')],
                                                    column: 5
                                                }},
                                                {{
                                                    rows: [getUrlParameter('directory_level_3')],
                                                    column: 6
                                                }},
                                                {{
                                                    rows: [getUrlParameter('filename_level_0')],
                                                    column: 7
                                                }},
                                                {{
                                                    rows: [getUrlParameter('filename_level_1')],
                                                    column: 8
                                                }},
                                                {{
                                                    rows: [getUrlParameter('filename_level_2')],
                                                    column: 9
                                                }},
                                                {{
                                                    rows: [getUrlParameter('filename_level_3')],
                                                    column: 10
                                                }},
                                                {{
                                                    rows: [getUrlParameter('filename_level_4')],
                                                    column: 11
                                                }},
                                                {{
                                                    rows: [getUrlParameter('filename_level_5')],
                                                    column: 12
                                                }}
                                            ]
                                        }},
                                        'columnDefs' : [
                                            {{
                                                'visible': false,
                                                'targets': [3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                                            }}
                                        ],
                                    }});
                                }});
                                </script>
                                {table}
                                <h6 align='right'> generated by <a href='https://gitlab.cern.ch/ludwig/cxaodtabulator'>CxAODTabulator v{__version__}</a></h6>
                                <h6 align='right'> maintainer: <a href='mailto:{_maintainer.mail}'>{_maintainer.username}, {_maintainer.mail}</a></h6>
                                </body>
                                </html>''')


def main(source: str = './data',
         target: str = './www',
         maintainer: dict = {'username': 'Steffen Ludwig',
                             'mail': 'steffen.ludwig@cern.ch'}):
    '''Generates a html overview for CxAODPlots.

    Args:
        source (str, optional): Sets default value for argparser. Defaults to './data'.
        target (str, optional): Sets default value for argparser. Defaults to './www'.
        maintainer (dict, optional): Sets default value for argparser. Required keys are 'username' and 'mail'. Defaults to {'username': 'Steffen Ludwig', 'mail': 'steffen.ludwig@cern.ch'}.
    '''

    # Parse args
    parser = argparse.ArgumentParser(
        description=f'Script to generate an html overview for CxAODPlots. Version v{__version__}.')
    parser.add_argument('--source',
                        action='store',
                        default=source,
                        dest='source',
                        help='sets input directory')
    parser.add_argument('--target',
                        action='store',
                        default=target,
                        dest='target',
                        help='sets output directory')
    parser.add_argument('--maintainer',
                        action='store',
                        default=maintainer,
                        dest='maintainer',
                        help="sets maintainer information; required keys are 'username' and 'mail'")
    args = parser.parse_args()

    # Set variables
    _source = args.source
    _target = args.target
    _maintainer = pd.Series(args.maintainer)

    # Copy files
    subprocess.run(f'rsync -chmPrz --delete --stats {_source}/ {_target}/',
                   shell=True, check=True)

    # Get data
    data = pd.DataFrame(
        {'raw': [_ for _ in glob.glob(f'{_target}/**/*.png', recursive=True)]})
    data['directory'] = data['raw'].str.split(
        '/').apply(lambda x: x[2:-1]).str.join('/')
    data['filename'] = data['raw'].str.split(
        '/').apply(lambda x: x[-1:][0]).str.replace('.png', '')

    # Get tags
    data['directory_tags'] = data.apply(
        lambda x: get_directory_tags(x.directory), axis=1)
    data['filename_tags'] = data.apply(
        lambda x: get_filename_tags(x.filename), axis=1)

    # Get preview
    data['preview'] = "<a href='" + data.directory + '/' + data.filename + \
        ".png'><img src='" + data.directory + '/' + \
        data.filename + ".png' height=200></a>"

    # Get links
    data['links'] = "<a href='" + data.directory + '/' + data.filename + ".png'>" + data.filename + \
        ".png</a><br>" + "<a href='" + data.directory + '/' + \
        data.filename + ".pdf'>" + data.filename + ".pdf</a><br>"

    # Get table
    table = pd.concat([
        data.directory,
        data.filename,
        data.preview,
        data.links,
        data.directory_tags.apply(pd.Series).sort_index(axis=1),
        data.filename_tags.apply(pd.Series).sort_index(axis=1),
    ], axis=1)

    # Filter log for BDTScore directories
    table.drop(table[~data.raw.str.contains('BDTScore')].query(
        "filename_level_9 == 'Log'").index, inplace=True)

    # Drop VBF 0-350 GeV
    table.drop(table.query(
        "filename_level_5 == 'VBFSR' & filename_level_2 == '0_350mHH'").index, inplace=True)

    # Reorder table
    table = table[[
        'directory',
        # 'filename',
        'preview',
        'links',
        'directory_level_0',
        'directory_level_1',
        'directory_level_2',
        'directory_level_3',
        # 'directory_level_4',
        # 'filename_level_0',
        'filename_level_1',
        'filename_level_2',
        # 'filename_level_3',
        'filename_level_4',
        # 'filename_level_5',
        'filename_level_6',
        'filename_level_7',
        'filename_level_8',
        'filename_level_9',
    ]].fillna('None')
    table.sort_values([
        'filename_level_2',
    ], ascending=[
        True,
    ], inplace=True)
    table = table.groupby([
        'directory',
        # 'filename',
        # 'preview',
        # 'links',
        'directory_level_0',
        'directory_level_1',
        'directory_level_2',
        'directory_level_3',
        # 'directory_level_4',
        # 'filename_level_0',
        'filename_level_1',
        # 'filename_level_2',
        # 'filename_level_3',
        'filename_level_4',
        # 'filename_level_5',
        'filename_level_6',
        'filename_level_7',
        'filename_level_8',
        'filename_level_9',
    ]).sum().reset_index()
    table = table[[
        'directory',
        # 'filename',
        'preview',
        'links',
        'directory_level_0',
        'directory_level_1',
        'directory_level_2',
        'directory_level_3',
        # 'directory_level_4',
        # 'filename_level_0',
        'filename_level_1',
        # 'filename_level_2',
        # 'filename_level_3',
        'filename_level_4',
        # 'filename_level_5',
        'filename_level_6',
        'filename_level_7',
        'filename_level_8',
        'filename_level_9',
    ]]

    # Set column names
    table.columns = [
        'directory',
        'preview (ggF 0-350 GeV, ggF >350 GeV, VBF)',
        'links',
        'directory level 0',
        'directory level 1',
        'directory level 2',
        'directory level 3',
        # 'directory level 4',
        'filename level 0',
        'filename level 1',
        'filename level 2',
        'filename level 3',
        'filename level 4',
        'filename level 5',
    ]

    # Save HTML
    with open(f'{_target}/index.html', 'w') as file:
        file.write(get_html(subtitle='',
                            data=table,
                            maintainer=_maintainer))


if __name__ == '__main__':
    main()
